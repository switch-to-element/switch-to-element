# Switch to Element!

https://element.vlaje.net/

***The Element onboarding guide for family members and non-tech friends.***

As simplified and straightforward as possible.

Some comments? -> element.entree929@aleeas.com or shoot an issue/MR right away.

Also, let me know if you like it and find useful! I'll be happy to hear that.

## Clone (Dev)

```bash
git clone git@gitlab.com:switch-to-element/switch-to-element.git
cd switch-to-element

git submodule update --init --recursive
```

## Sources (Dev)

- https://imfing.github.io/hextra/
- https://v1.heroicons.com/
- https://gitlab.com/pages/hugo/-/tree/main?ref_type=heads
- https://gohugo.io/documentation/
