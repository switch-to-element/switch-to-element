---
linkTitle: Proč?
title: Proč
weight: 4
toc: false
---

bych se měl/a obtěžovat s měněním messengeru?

## Protože jsme na cestě do digitálního gulagu.

Big tech firmy jako Google, Facebook, Microsoft, Amazon... o nás sbírají obrovské množství dat, na jejichž základě si vytváří náš velmi přesný profil.

{{< callout type="info" emoji=false >}}
  **Personalizovaná reklama**, kterou nás krmí, je tím nejméně škodlivým, i když dost otravným důsledkem.
{{< /callout >}}

{{< callout type="warning" emoji=false >}}
  **Personalizované feedy**, které nám brání odtrhnout se od displeje, jsou už horší. Algorytmům postaveným na důmyslných psychologických technikách a tom, co o nás platforma ví, dokáže odolávat málokdo.
{{< /callout >}}

{{< callout type="error" emoji=false >}}
  **Společenské, politické a mocenské důsledky** umožněné cílením informací na přesně vymezené skupiny lidí a současnou možností cenzury skupin jiných, jsou nejzávažnější. Obyčejnému člověku sice nemusí být přímo patrné každý den, přesto se každý den dějí a ovlivňují jej.
{{< /callout >}}

Mainstreamové platformy také aktivně spolupracují s vládami, tudíž pokud se politická situace nebo tvoje názory nevyvinou správným směrem, tohle je další riziko.

## Chci zpět důstojnost

Představa, že můžu věřit tomu, že konverzace, kterou s druhým vedu, je skutečně soukromá a nikdo jiný nemá možnost ji číst a analyzovat v můj neprospěch, mi vrací klid a důstojnost.

Nevím, jestli to máš podobně, ale já si všímám, že neustálé vědomí, že mě někdo sleduje, analyzuje a vyhodnocuje, jak mi něco nacpat, jak mě zmanipulovat nebo podvést, je pro mě destruktivní.

A postupem času vidím, že tendence jsou horší a horší. Zneužívání dat a manipulace jsou každodenní realitou. Reklamy a algorytmy jsou vlezlejší a agresivnější. Ze všech stran vnímám úpornou snahu mě dostat tam, kam se to té které platformě hodí, a uzamknout mě. Je mi to odporné, je mi z toho zle a nežije se mi v tom dobře.

**To jsou moje důvody, proč hledám svobodné alternativy. Budu rád, když se připojíš.**

<br/>

{{< cards >}}
  {{< card link="/element-is-better" title="Element je lepší" icon="shield-check" subtitle="Lepší než WhatsApp, Telegram i&nbsp;Signal" >}}
  {{< card link="/data-abuse" title="Zneužívání dat" icon="thumb-down" subtitle="big tech firmami. Jak funguje + příklady" >}}
{{< /cards >}}
