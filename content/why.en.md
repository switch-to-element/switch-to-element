---
linkTitle: Why?
title: Why
weight: 4
toc: false
---

should I bother changing my messenger?

## Because we're on a way to the digital gulag.

Big tech companies like Google, Facebook, Microsoft, Amazon... collect a huge amount of data about us, based on which they create a very accurate profile of us.

{{< callout type="info" emoji=false >}}
  **Personalised ads** are the least damaging, although very annoying consequence.
{{< /callout >}}

{{< callout type="warning" emoji=false >}}
  **Personalised feeds** keeping us stuck on the screen are worse. Few can resist algorithms built on sophisticated psychological techniques combined with all what the platform knows about us.
{{< /callout >}}

{{< callout type="error" emoji=false >}}
  **Social, political and power implications** made possible by targeting information to well-defined groups of people and the ability of censoring others at the same time are the most serious ones. While they may not be obvious to the ordinary person every day, they do happen and affect them.
{{< /callout >}}

Mainstream platforms also actively collaborate with governments so if the political situation or your views go sideways, this is another risk.

## I want back my dignity

The idea that I can trust that my conversations are truly private and nobody else can read and analyze them to my disadvantage brings me back peace and dignity.

I notice that the constant awareness of someone analyzing and evaluating how to manipulate or screw me over is destructive to me.

And as time goes on I see the tendencies are only getting worse. Data abuse and aggresive manipulation are a fact. Ads and algorithms are more and more intrusive. Platforms put relentless effort to get me where it suits them and lock me there. It's disgusting and it makes me sick.

**These are my reasons to look for free alternatives. I'd be happy if you join me.**

<br/>

{{< cards >}}
  {{< card link="/en/element-is-better" title="Element is better" icon="shield-check" subtitle="Better than WhatsApp, Telegram, Signal..." >}}
  {{< card link="/en/data-abuse" title="Data abuse" icon="thumb-down" subtitle="by big tech companies. How does it work + examples" >}}
{{< /cards >}}
