---
linkTitle: Zneužívání dat
title: Jak funguje zneužívání dat big tech firmami
weight: 5
---

## Sběr, kombinování a analýza

Big tech korporace typicky provozují paletu služeb a data, která z nich získávají, kombinují.

To platí jak pro Metu (Facebook, WhatsApp, Instagram), tak Google, Microsoft a další.

Představ si firmu, od níž využíváš modrou sociální síť + růžový sdíleč obrázků a videí + zelený/fialový messenger.

Firma všechny informace, které o tobě má, sesype na jednu hromadu, důmyslně je proskenuje a vytvoří si tvůj přesný profil.

- Veškteré **konverzace**, které jsi v jejích službách vedl/a od založení svých účtů - od komentářů až po mnoho let staré *soukromé* zprávy
  - Jaká slova a jak často používáš, jaká jsou *tvá* témata, jaký máš styl jazyka, s kým komunikuješ a jak se to všechno vyvíjí v čase
- **Chování** - co lajkuješ, sdílíš, nad čím trávíš více a nad čím méně času
  - Na jaké odkazy klikáš a jaké weby navštěvuješ (firma tě sleduje i na spoustě webů, které s ní přímo nesouvisí)
- **Metadata** - kde se pohybuješ, kdy se připojuješ, jaká zařízení používáš...
- **Profily těch, se kterými jsi ve spojení** - ty jsou také vydatným zdrojem informací o tobě

Na analýzu těchto dat má firma ty nejlepší lidi, neomezený výpočetní výkon a finance. Profil, který si o tobě udělá, je dokonalý.

## Data znamenají moc

Moc, která z těchto dat plyne, je obrovská. Nejenže tě firma může krmit obsahem, vůči kterému máš nejmenší odolnost, ale na druhé straně může i cíleně cenzurovat, co k tobě pustí. To všechno ideálně zcela bez tvého vědomí.

Stejná rizika platí i pro jiné big techy, které provozují mailové služby, cloudová úložiště, reklamní a operační systémy, kancelářské nástroje a ostatní sociální sítě. Dnešní big tech svět je kontrolovaný finančními skupinami, jejichž cílem je co nejvyšší zisk bez ohledu na širší důsledky.

Problémem nadměrného hromadění dat je také riziko jejich úniku. Žádná služba by neměla sbírat více dat, než je nezbytně nutné k jejímu fungování. V ideálním světě.

Pokud se chceš alespoň pokusit nebýt obětí tohoto systému, je třeba hledat alternativy, které ti nebudou nasazovat okovy a nebudou tě vysávat.

## Příklady zneužívání

Jestli se o praktikách big techu chceš dozvědět/přesvědčit víc, kauz a informací jsou spousty...

- **The Facebook Files/Papers** - Interní dokumenty o tom, že Facebook má informace potvrzující mj. negativní vliv jeho služeb na psychiku mladistvých a rozkladné vlivy na společnosti v rozvojových zemích, a vědomě je ignoruje - [cbsnews.com](https://www.cbsnews.com/news/facebook-whistleblower-frances-haugen-misinformation-public-60-minutes-2021-10-03/), [businessinsider.com](https://www.businessinsider.com/facebook-papers-mark-zuckerberg-frances-haugen-leaked-docs-2021-10), [wsj.com](https://www.wsj.com/articles/the-facebook-files-11631713039)
- **Cambridge Analytica** - Masivní zneužití dat v politických kampaních - [businessinsider.com](https://www.businessinsider.com/cambridge-analytica-trump-firm-facebook-data-50-million-users-2018-3), [wikipedia.org](https://en.wikipedia.org/wiki/Facebook%E2%80%93Cambridge_Analytica_data_scandal)
- [**Zneužití cílené reklamy Evropskou komisí** za účelem politické manipulace](https://echo24.cz/a/HMrtc/zpravy-domaci-narizeni-evropska-komise-ochrana-deti-ztrata-soukromi-internet)
- **Covidová cenzura** podle pokynů americké administrativy - [odysee.com](https://odysee.com/@WatchingTheWatchers:8/facebook-files-1-biden-pressured-zuck-to:4), [youtube.com](https://www.youtube.com/watch?v=oBqvTH-dE4E)
- atd. atd. atd... [Criticism of Facebook](https://en.wikipedia.org/wiki/Criticism_of_Facebook)

<br/>

{{< cards >}}
  {{< card link="/element-is-better" title="Element je lepší" icon="shield-check" subtitle="Lepší než WhatsApp, Telegram i&nbsp;Signal" >}}
{{< /cards >}}
