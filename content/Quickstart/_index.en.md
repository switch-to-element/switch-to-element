---
linkTitle: Quickstart
title: Quickstart
weight: 1
toc: false
---

{{< cards >}}
  {{< card link="/en/quickstart/mobile" title="Mobile" icon="device-mobile" >}}
  {{< card link="/en/quickstart/desktop" title="Desktop (web)" icon="desktop-computer" >}}
{{< /cards >}}
