---
linkTitle: Mobile
weight: 1
---

{{< callout type="info" >}}

  **Prepare paper and pencil.** You'll need to write down a security key that looks something like:

  EsTU wicT NjeF 7cA8 n6NM hjqy RvDZ nYFW MwNd SCQD iscU JRuc

  You can store it in your password manager, but **if you're serious about encryption and privacy, don't put it anywhere else**.

{{< /callout >}}

{{% steps %}}

### Download the app

https://element.io/download

### Launch it, choose `Create Account`

- Others will search you as **@\<USERNAME\>:matrix.org** (so I'd go for something short but wouldn't use a full real name)

### Enter and confirm email

- Used for account verification
- By default nobody will be able to search you with it

### Back up your Security Key

1. Go to the main screen (so far it's empty)
2. Tap the round icon with your initial at the top left
3. **`Security & Privacy`** -> Secure Backup -> **`Set up Secure Backup`** -> **`Use a Security Key`**
   - If you don't see the "Secure Backup" menu, finish the installation and have a look [here](/en/known-issues)
4. Write the displayed key on a piece of paper and put it between your birth certificate and your high school diploma. Alternatively, you can save the key in your password manager.

{{< callout >}}

This is important. The broader context [here](/en/security-key).

{{< /callout >}}

5. After you write down the key it's impossible to return to the home screen. You have to kill the app and reopen it. The backup is created with this however.

### Initial settings

- Icon with your initial at the top left -> **`Notifications`**
  - **Email notifications**
    - You probably want to turn this off. In my experience, email notifications don't work reliably anyway.
- Sometimes Element may not show a notification when you receive a message. In such case, a few more measures are needed, see [here](/en/known-issues).

### Find others and start chatting

- Pencil icon on the bottom right -> **`Start Chat`**
- Search others as **@\<USERNAME\>:matrix.org**

{{< callout type="info" >}}
  Try it with `@vlaje-test:matrix.org`. It's my test account. I may or may not approve you and reply at some point.
{{< /callout >}}

- As soon as you send the first message to the person, they'll receive a chat invitation

{{% /steps %}}

## Next steps

{{< cards >}}
  {{< card link="/en/security-key" title="Security Key" subtitle="Info and context" >}}
  {{< card link="/en/link-another-device" title="Link Other Device" subtitle="How to do it" >}}
  {{< card link="/en/known-issues" title="Known Issues" subtitle="In case of any problems, take a look here" >}}
{{< /cards >}}
