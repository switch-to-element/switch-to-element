---
linkTitle: Na mobilu
weight: 1
---

{{< callout type="info" >}}

  **Připrav si papír a tužku.** Bude potřeba si zapsat bezpečnostní klíč, který bude vypadat nějak takhle:

  EsTU wicT NjeF 7cA8 n6NM hjqy RvDZ nYFW MwNd SCQD iscU JRuc

  Můžeš si ho případně uložit do správce hesel, ale **pokud to s šifrováním a soukromím myslíš vážně, jinam ho nedávej**.

{{< /callout >}}

{{% steps %}}

### Stáhni appku

https://element.io/download

### Spusť jí, dej `Vytvořit účet`

- Ostatní tě budou vyhledávat jako **@<UŽIVATELSKÉ_JMÉNO>:matrix.org** (takže bych volil něco krátkýho, ale nedával bych asi reálný jméno nebo příjmení)

### Zadej a potvrď mail

- Slouží pro ověření účtu
- Pokud necháš povolené (v jednom z dalších kroků), tak ti na něj můžou chodit oznámení o nových zprávách
- Ve výchozím stavu tě podle něj ale nikdo nenajde

### Zazálohuj bezpečnostní klíč

1. Přejdi na hlavní obrazovku aplikace (zatim je prázdná)
2. Tapni na kulatou ikonu se svým iniciálem vlevo nahoře
3. **`Zabezpečení a soukromí`** -> Bezpečná záloha -> **`Vytvořit bezpečnou zálohu`** -> **`Použít bezpečnostní klíč`**
   - Pokud nabídku "Bezpečná záloha" nevidíš, dokonči instalaci a pak se podívej [sem](/known-issues)
4. Zobrazený klíč si přepiš na papír a ten dej mezi rodnej list a maturitní vysvědčení. Případně si klíč můžeš uložit do password manageru.

{{< callout >}}

Tohle je důležitý. Širší kontext [zde](/security-key).

{{< /callout >}}

5. Po opsání klíče se nedá vrátit zpět na hlavní obrazovku, takže je potřeba aplikaci zabít a znovu otevřít. Záloha je tímto ale vytvořená.

### Úvodní nastavení

- Ikona s iniciálem vlevo nahoře -> **`Oznámení`**
  - **Oznámení e-mailem**
    - Tohle chceš pravděpodobně vypnout. Z mých zkušeností oznámení do mailu stejně nefungují spolehlivě.
- Někdy se může stát, že Element nezobrazuje oznámení, když ti přijde zpráva. V takovém případě je potřeba ještě pár opatření, viz [zde](/known-issues).

### Najdi ostatní a začni chatovat

- Vpravo dole ikona s tužkou -> **`Zahájit konverzaci`**
- Ostatní hledej jako **@<UŽIVATELSKÉ_JMÉNO>:matrix.org**

{{< callout type="info" >}}
  Můžeš zkusit `@vlaje-test:matrix.org`. To je můj testovací účet. Možná tě tam někdy schválím a odpovím.
{{< /callout >}}

- Jakmile odešleš dotyčnému první zprávu, přijde mu pozvánka ke konverzaci

{{% /steps %}}

## Další kroky

{{< cards >}}
  {{< card link="/security-key" title="Bezpečnostní klíč" subtitle="Info a kontext" >}}
  {{< card link="/link-another-device" title="Napoj další zařízení" subtitle="Jak na to" >}}
  {{< card link="/known-issues" title="Známé problémy" subtitle="Pokud se vyskytly nějaké problémy, koukni sem" >}}
{{< /cards >}}
