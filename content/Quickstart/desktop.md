---
linkTitle: Na kompu
title: Vytvoření účtu z webového prohlížeče
weight: 2
---

{{< callout type="info" >}}

  **Připrav si papír a tužku.** Bude potřeba si zapsat bezpečnostní klíč, který bude vypadat nějak takhle:

  EsTU wicT NjeF 7cA8 n6NM hjqy RvDZ nYFW MwNd SCQD iscU JRuc

  Můžeš si ho případně uložit do správce hesel, ale **pokud to s šifrováním a soukromím myslíš vážně, jinam ho nedávej**.

{{< /callout >}}

{{% steps %}}

### Otevř Element Web

https://app.element.io/

### Dej `Vytvořit účet`

- Ostatní tě budou vyhledávat jako **@<UŽIVATELSKÉ_JMÉNO>:matrix.org** (takže bych volil něco krátkýho, ale nedával bych asi reálný jméno nebo příjmení)

### Zadej a potvrď mail

- Slouží pro ověření účtu
- Ve výchozím stavu tě podle něj nikdo nenajde

### Zazálohuj bezpečnostní klíč

1. Přejdi na hlavní obrazovku
2. Tapni na kulatou ikonu se svým iniciálem vlevo nahoře
3. **`Zabezpečení a soukromí`** -> Šifrování -> Bezpečná záloha -> **`Nastavit`** -> **`Vygenerovat bezpečnostní klíč`**
4. Zobrazený klíč si přepiš na papír a ten dej mezi rodnej list a maturitní vysvědčení. Případně si klíč můžeš uložit do password manageru.

{{< callout >}}

Tohle je důležitý. Širší kontext [zde](/security-key).

{{< /callout >}}

5. I když si klíč opíšeš, tak pro dokončení nastavení je potřeba kliknout na **`Zkopírovat`**. Teprve pak se odblokuje **`Pokračovat`** a je možné nastavení dokončit.

### Úvodní nastavení

- Ikona s iniciálem vlevo nahoře -> **`Oznámení`**
  - **Povolení e-mailových oznámení...**
    - Tohle chceš pravděpodobně vypnout. Z mých zkušeností oznámení do mailu stejně nefungují spolehlivě.

### Najdi ostatní a začni chatovat

- V levém sloupci u "Lidé" klikni na **`+`** vpravo ("Zahájit konverzaci")
- Ostatní hledej jako **@<UŽIVATELSKÉ_JMÉNO>:matrix.org**

{{< callout type="info" >}}
  Můžeš zkusit `@vlaje-test:matrix.org`. To je můj testovací účet. Možná tě tam někdy schválím a odpovím.
{{< /callout >}}

- Jakmile odešleš dotyčnému první zprávu, přijde mu pozvánka ke konverzaci

{{% /steps %}}

## Další kroky

{{< cards >}}
  {{< card link="/security-key" title="Bezpečnostní klíč" subtitle="Info a kontext" >}}
  {{< card link="/link-another-device" title="Napoj další zařízení" subtitle="Jak na to" >}}
  {{< card link="/known-issues" title="Známé problémy" subtitle="Pokud se vyskytly nějaké problémy, koukni sem" >}}
{{< /cards >}}
