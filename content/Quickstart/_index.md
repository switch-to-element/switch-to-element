---
linkTitle: Rychlostart
title: Rychlostart
weight: 1
toc: false
---

{{< cards >}}
  {{< card link="/quickstart/mobile" title="Na mobilu" icon="device-mobile" >}}
  {{< card link="/quickstart/desktop" title="Na kompu (prohlížeč)" icon="desktop-computer" >}}
{{< /cards >}}
