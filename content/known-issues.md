---
linkTitle: Známé problémy
title: Známé problémy
weight: 7
toc: false
---

{{% details title="Zobrazuje se mi varování \"Máte neověřené relace\"" closed="true" %}}

Většinou se jedná o pozůstatek po pokusech o přihlášení, při kterých jsi nedokončil/a ověření, a tím pádem falešné varování.

K této situaci dojde, když se přihlásíš pomocí jména a hesla, ale už nedokončíš ověření relace.

Pokud ses v nedávné době na žádném zařízení nepřihlašoval/a, můžeš si prohlédnout seznam neověřených relací a ujistit se, že mezi nimi není něco podezřelého (**`Prohlédnout`** -> **`Zobrazit všechny`**).

Ve výpisu relací vidíš, které jsou neověřené, z jakého jsou zařízení, data a IP adresy.

Obecně špatně by bylo, kdyby se ti do účtu přihlásil někdo z typu zařízení, který nepoznáváš, neznámé IP adresy* a/nebo v dobu, kdy ses určitě nepřihlašoval/a...

Nic nezkazíš tím, když klikneš na neověřenou relaci a dáš **`Odhlásit se z této relace`**.

Pokud by ti toto varování vyskakovalo opakovaně a bez zřejmé příčiny, znamenalo by to, že někdo má tvé jméno a heslo, přihlašuje se ti do účtu, ale nemá bezpečnostní klíč a není schopen obash účtu dešifrovat.

_*) IP adresy se mění v závislosti na tom, odkud se přihlašuješ, zda používáš pevný nebo mobilní internet, Wi-Fi mimo svůj domov, VPN atd..._

{{% /details %}}

{{% details title="Nechodí mi oznámení" closed="true" %}}

Na některých mobilech se může objevit problém s tím, že nechodí oznámení, nebo chodí nespolehlivě.

Zatím jsem se s tím setkal jen u Samsungů.

Obecně je třeba zamezit tomu, aby Android "optimalizoval" využívání baterie Elementem.

Toto se dá nastavit (v závislosti na zařízení a verzi Androidu) dlouhým podržením ikony Elementu, tapnutím na ikonu "{{< icon "information-circle" >}}" (info), což nás dostane do nastavení aplikace (na úrovni Androidu). Tam je potřeba zvolit **`Pokročilé`** -> **`Baterie`** -> **`Optimalizace (využívání) baterie`** a optimalizaci pro Element vypnout.

Element má taktéž zabudovanou funkci pro řešení problémů s oznámeními - **`Nastavení`** -> **`Oznámení`** -> úplně dole **`Odstraňování problémů s oznámeními`**.

{{% /details %}}

{{% details title="Nevidím možnost \"Bezpečná záloha\"" closed="true" %}}

Setkal jsem se se situací, kdy se mi po instalaci aplikace a založení účtu nezobrazila možnost "Bezpečná záloha" potřebná k vygenerování bezpečnostního/šifrovacího klíče.

Stalo se mi to u aplikace nainstalované přes FDroid.

Mám pocit, že se možnost objevila později, ale zatím nemám přesný postup, jak situaci řešit.

Pokud k tomu máš nějaké poznatky, napiš mi je na element.entree929@aleeas.com.

{{% /details %}}
