---
cascade:
  type: docs
toc: false
---

<div class="mb-8">
{{< hextra/hero-headline >}}
  Dump mainstream messengers&nbsp;<br class="sm:block hidden" />and&nbsp;switch to Element
{{< /hextra/hero-headline >}}
</div>

<div class="mb-8">
{{< hextra/hero-subtitle >}}
  **Privacy.** Encrypted communication which you control. No snooping, personalized ads or&nbsp;manipulation.
{{< /hextra/hero-subtitle >}}
</div>

<div class="mb-12">
{{< hextra/hero-subtitle >}}
  **Independence** from one particular company, transparency, decentralisation.
{{< /hextra/hero-subtitle >}}
</div>

<div class="mb-6">
{{< hextra/hero-button text="Quickstart" link="/en/quickstart" >}}
</div>

<br/>

{{< cards >}}
  {{< card link="/en/why" title="Why?" icon="question-mark-circle" subtitle="Why should I bother changing my messenger?" >}}
  {{< card link="/en/element-is-better" title="Element is better" icon="shield-check" subtitle="Better than WhatsApp, Telegram, Signal..." >}}
{{< /cards >}}
