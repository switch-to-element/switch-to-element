---
linkTitle: Security Key
title: Security/Encryption Key
weight: 2
---

To fully log into Element, a username and password are not enough, you also need a security key.

This is because the messages are encrypted and no one else has the instructions on how to decrypt them. The Security/Encryption Key is that instruction.

That's why, unlike mainstream messengers, you need to save and store it yourself. If you keep it secret, you are guaranteed that no one can get to the contents of your account.

## How to store the Key properly

The most straightforward way is to write the Key down and store it among other important paper documents.

It's also fine to store it in a password manager, although be careful here because if someone gets into your password manager, they'll have all 3 pieces of information needed to take over your account (name, password and key).

## How not to store the Key

Definitely don't put it on any cloud (Google Drive, Google Photos, OneDrive, DropBox, iCloud...).

Once something is in the cloud, you'll never have control over it again. Just because you delete something from the cloud doesn't mean it's deleted from the server. You just can't see it.

Don't take a screenshot or a photo of the Key. From there it's just a step to (automatically) uploading it to the cloud.
