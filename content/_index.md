---
cascade:
  type: docs
toc: false
---

<div class="mb-8">
{{< hextra/hero-headline >}}
  Zahoď mainstreamové messengery&nbsp;<br class="sm:block hidden" />a&nbsp;přejdi na Element
{{< /hextra/hero-headline >}}
</div>

<div class="mb-8">
{{< hextra/hero-subtitle >}}
  **Soukromí.** Šifrovaná komunikace, nad kterou máš kontrolu.&nbsp;<br class="sm:block hidden" />Žádné šmírování, personalizované reklamy a manipulace.
{{< /hextra/hero-subtitle >}}
</div>

<div class="mb-12">
{{< hextra/hero-subtitle >}}
  **Otevřenost.** Nezávislost na jedné konkrétní firmě, transparentnost, decentralizace.
{{< /hextra/hero-subtitle >}}
</div>

<div class="mb-6">
{{< hextra/hero-button text="Rychlostart" link="/quickstart" >}}
</div>

<br/>

{{< cards >}}
  {{< card link="/why" title="Proč?" icon="question-mark-circle" subtitle="Proč bych se měl/a obtěžovat s měněním messengeru?" >}}
  {{< card link="/element-is-better" title="Element je lepší" icon="shield-check" subtitle="Lepší než WhatsApp, Telegram i&nbsp;Signal" >}}
{{< /cards >}}
