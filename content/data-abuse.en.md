---
linkTitle: Data Abuse
title: How does data abuse by big tech companies work
weight: 5
---

## Collection, combining and analysis

Big tech corporations typically run a variety of services and combine the data they collect from them.

That applies to Meta (Facebook, WhatsApp, Instagram), Google, Microsoft and others.

Imagine a company from which you are using a blue social network + a pink picture and video sharing service + a green/purple messenger.

The company puts all the information it has about you into one pile, ingeniously scans it and creates your perfect profile.

- All **conversations** you've had on their services since you created your accounts - from comments to many-year-old *private* chats
  - What words are you using and how often, what are *your* topics, what is your language style, who are you interacting with and how does that all evolve over the time
- **Behavior** - what are you liking, sharing, spending more and less time with
  - What links you are clicking and what sites you are visiting (the company is also tracking you on many sites not directly related to them)
- **Metadata** - what's your location, when are you connecting, what devices are you using...
- **Profiles of others you interact with** - these are also a significant source of information about you

To analyze this data, the company has the best people, unlimited computing power and money. The profile they can build about you is brilliant.

## Data means power

The power derived from such data is enormous. Not only can the company feed you content that you are the least resistant to but on the other hand they also can cleverly censor what you'll see. Everything without your knowledge ideally.

The same risks apply to other big techs that run mail services, cloud storages, advertising and operating systems, office tools and other social networks. Today's big tech world is controlled by financial interests whose aim is to maximise profits with only little consideration of the wider implications.

Another problem of excessive data collection is the risk of leakage. No service should collect more data than absolutely necessary for it to function. In an ideal world.

If you want to at least try not to become a victim of this system you need to look for alternatives that won't be exploiting and locking you in.

## Examples of abuse

If you want to find out more about big tech practices, there are plenty of cases and sources...

- **The Facebook Files/Papers** - Internal documents that Facebook knows very well about, among other things, the negative impacts of its services on mental health of adolescents and the corrosive effects on societies in different countries, and is purposely ignoring it - [cbsnews.com](https://www.cbsnews.com/news/facebook-whistleblower-frances-haugen-misinformation-public-60-minutes-2021-10-03/), [businessinsider.com](https://www.businessinsider.com/facebook-papers-mark-zuckerberg-frances-haugen-leaked-docs-2021-10), [wsj.com](https://www.wsj.com/articles/the-facebook-files-11631713039)
- **Cambridge Analytica** - Massive Facebook data abuse in political campaigns - [businessinsider.com](https://www.businessinsider.com/cambridge-analytica-trump-firm-facebook-data-50-million-users-2018-3), [wikipedia.org](https://en.wikipedia.org/wiki/Facebook%E2%80%93Cambridge_Analytica_data_scandal)
- [**Abuse of targeted advertising by the European Commission** for political manipulation](https://echo24.cz/a/HMrtc/zpravy-domaci-narizeni-evropska-komise-ochrana-deti-ztrata-soukromi-internet)
- **Covid censorship** directed by the US administration - [odysee.com](https://odysee.com/@WatchingTheWatchers:8/facebook-files-1-biden-pressured-zuck-to:4), [youtube.com](https://www.youtube.com/watch?v=oBqvTH-dE4E)
- etc. etc. etc... [Criticism of Facebook](https://en.wikipedia.org/wiki/Criticism_of_Facebook)

<br/>

{{< cards >}}
  {{< card link="/en/element-is-better" title="Element is better" icon="shield-check" subtitle="Better than WhatsApp, Telegram, Signal..." >}}
{{< /cards >}}
