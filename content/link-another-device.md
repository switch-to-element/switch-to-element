---
linkTitle: Napoj další zařízení
title: Napojení dalšího zařízení
weight: 3
toc: false
---

Každé nové přihlášení je třeba ještě ověřit.

Nejjednodušší je to z jiného zařízení, na kterém jsi již přihlášen/a.





{{% details title="Mobil -> Počítač" closed="true" %}}

_Jsem přihlášen/a na mobilu a chci se přihlásit na počítači_

||||
|-|-|-|
| 1 | na&nbsp;počítači | Přihlaš se klasicky jménem a heslem, objeví se výzva **Ověřit toto zařízení** |
| 2 | na&nbsp;počítači | Pokud máš bezpečnostní klíč uložen ve správci hesel a můžeš ho snadno překopírovat, zvol **`Ověření pomocí bezpečnostního klíče`**, jinak dej **`Ověřit pomocí jiného zařízení`** |
| 3 | na&nbsp;mobilu   | Vyskočí ti lišta **Žádost na ověření**, tapni na ní a dej **`Přijmout`** |
| 4 | na&nbsp;počítači | Zobrazí se ti QR kód |
| 5 | na&nbsp;mobilu   | Objeví se možnost **`Oskenovat tímto zařízením`**, oskenuj QR kód |
| 6 | na&nbsp;počítači | Potvrď, jestliže se ti na mobilu zobrazil stejný obrázek |

{{% /details %}}





{{% details title="Počítač -> Mobil" closed="true" %}}

_Jsem přihlášen/a na počítači a chci se přihlásit v mobilu_

||||
|-|-|-|
| 1 | na&nbsp;mobilu   | Přihlaš se do aplikace klasicky jménem a heslem |
| 2 | na&nbsp;mobilu   | Vyskočí ti lišta **Ověřit toto zařízení**, tapni na ní |
| 3 | na&nbsp;mobilu   | Pokud máš bezpečnostní klíč uložen ve správci hesel a můžeš ho snadno překopírovat, zvol **`Ověření pomocí bezpečnostního klíče`**, jinak dej **`Ověřit pomocí jiného zařízení`** |
| 4 | na&nbsp;počítači | Vyskočí ti **Žádost o ověření**; klikni na **`Ověřit relaci`**, zobrazí se ti QR kód |
| 5 | na&nbsp;mobilu   | Objeví se možnost **`Oskenovat tímto zařízením`**, oskenuj QR kód |
| 6 | na&nbsp;počítači | Potvrď, jestliže se ti zobrazil stejný obrázek |
| 7 | na&nbsp;mobilu   | Konverzace v účtu se nemusí hned načíst. Pokud své konverzace nevidíš, zavři (zabij) Element a znovu jej otevři. |

{{% /details %}}





{{% details title="Počítač -> Počítač" closed="true" %}}

_Jsem přihlášen/a na počítači a chci se přihlásit na další počítač_

||||
|-|-|-|
| 1 | Nový počítač       | Přihlaš se klasicky jménem a heslem, objeví se výzva **Ověřit toto zařízení** |
| 2 | Nový počítač       | Pokud máš bezpečnostní klíč uložen ve správci hesel a můžeš ho snadno překopírovat, zvol **`Ověření pomocí bezpečnostního klíče`**, jinak dej **`Ověřit pomocí jiného zařízení`** |
| 3 | Přihlášený počítač | Vyskočí ti oznámení **Žádost ověření**, klikni na **`Ověřit relaci`** |
| 4 | Oba počítače       | Objeví se okno s výzvou **Porovnejte jedinečnou kombinaci emoji** |
| 5 | Oba počítače       | Zkontroluj a potvrď, jestliže se emoji a jejich pořadí na obou počítačích shodují |

_(Nechápu, jak tenhle způsob ověření/přenosu bezpečnostního klíče funguje. Pokud mi to někdo dokáže vysvětlit, napište prosím na element.entree929@aleeas.com)_

{{% /details %}}





{{% details title="Mobil -> Mobil" closed="true" %}}

_Jsem přihlášen/a na mobilu a chci se přihlásit na další mobil_

||||
|-|-|-|
| 1 | Nový mobil       | Přihlaš se do aplikace klasicky jménem a heslem |
| 2 | Nový mobil       | Vyskočí ti lišta **Ověřit toto zařízení**, tapni na ní |
| 3 | Nový mobil       | Pokud máš bezpečnostní klíč uložen ve správci hesel a můžeš ho snadno překopírovat, zvol **`Použijte metodu obnovy`**, jinak dej **`Ověřit pomocí jiného zařízení`** |
| 4 | Přihlášený mobil | Vyskočí ti lišta **Žádost na ověření**, tapni na ní a dej **`Přijmout`**, na obou mobilech se zobrazí QR kód |
| 5 | Jakýkoliv mobil  | Objeví se možnost **`Oskenovat tímto zařízením`** na obou mobilech, oskenuj QR jedním z druhého |
| 6 | Druhý mobil      | Potvrď, jestliže se ti na obou mobilech zobrazil stejný obrázek |
| 7 | Nový mobil       | Konverzace v účtu se nemusí hned načíst. Pokud své konverzace nevidíš, zavři (zabij) Element a znovu jej otevři. |

{{% /details %}}





{{% details title="Aktuálně nejsem nikde přihlášen/a" closed="true" %}}

V takovém případě je jediná možnost ověření vložením bezpečnostního klíče.

{{% /details %}}





## Proč je třeba se ověřovat?

Po každém přihlášení k účtu je třeba přenést/vložit bezpečnostní klíč, aby mohl být dešifrován jeho obsah.

"Ověření" není nic jiného než přenos tohoto klíče, akorát přívětivějším způsobem.

Pokud nejsi přihlášen/a nikde, nezbyde ti než vložit celý bezpečnostní klíč.
