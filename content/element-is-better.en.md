---
linkTitle: Element Is Better
weight: 6
---

{{< hextra/feature-grid >}}
  {{< hextra/feature-card
    title="Decentralized"
    subtitle="You can choose with which provider you'll register your account. You can access your account with other apps than just Element."
  >}}
  {{< hextra/feature-card
    title="Privacy protecting"
    subtitle="Content of your account is encrypted. You only enter email when registering. It doesn't need access to your contacts. It doesn't collect more information than necessary."
  >}}
  {{< hextra/feature-card
    title="Open"
    subtitle="It's possible to verify what the application actually does. If the current developer started to behave unethically it's possible and legal to duplicate the app and discard the bad changes."
  >}}
{{< /hextra/feature-grid >}}

<br/>

# Element is better than -

## WhatsApp (and Facebook Messenger)

- It's owned by Meta (Facebook) - you can read more about what Meta is like [here](/en/data-abuse)
- While the conversations content *should* be encrypted, Meta definitely accesses your metadata and contacts
  - **Metadata** like *who* you communicate with, *how often*, *from which devices, IP addresses*... are also highly sensitive personal data
  - If the app isn't open source it's impossible to verify that the provider doesn't have access to your conversations despite the end-to-end encryption. Information exists that the conversations are monitored anyway (see below).
- **You can't use the app without accessing your phone contacts** - Meta enforces access to your current contact list
- Facebook's takeover of WhatsApp is a sad story culminating in the WA founder's recommendation to "Delete Facebook" and his support to Signal as a rival platform

{{% details title="More arguments against WhatsAppu" closed="true" %}}

- DON'T Use WhatsApp! - [peertube.tv](https://peertube.tv/w/8suSZ4ojg2Q2ok8HMmn6T5), [youtube.com](https://www.youtube.com/watch?v=shpiVm1qpnw)
- [Proof that WhatsApp isn't private!](https://www.youtube.com/watch?v=sqb37P4vbrc&t=130s)
- How Whatsapp (and Facebook) will Zuck You - [odysee.com](https://odysee.com/@RobBraxmanTech:6/how-whatsapp-and-facebook-will-zuck-you:e), [youtube.com](https://www.youtube.com/watch?v=I6Fbjr2WfFw)
- [Why You Should Delete Whatsapp](https://www.youtube.com/watch?v=bP5nXvzZMcE)
- [Delete WhatsApp Now](https://www.youtube.com/watch?v=KAsoDwpO2OU)
- Background of Facebook's takeover of WhatsApp - [buzzfeednews.com](https://www.buzzfeednews.com/article/ryanmac/whatsapp-brian-acton-delete-facebook-stanford-lecture), [forbes.com](https://www.forbes.com/sites/parmyolson/2018/09/26/exclusive-whatsapp-cofounder-brian-acton-gives-the-inside-story-on-deletefacebook-and-why-he-left-850-million-behind/)

{{% /details %}}

## Telegram

- It isn't better than WhatsApp or Facebook Messenger in principle - it's a closed centralized platform
- Conversations aren't encrypted by default thus Telegram can access them
- Its scope is too wide, it overwhelms with unnecessary features thus it's hard to navigate the settings and keep track of the important ones
- It requires a phone number

## Signal

- Privacy/security goes too much at the expense of usability here (for our purposes)
  - For me the main problem is the inability to sync more than 2 devices (and even that didn't work well)
    - The fact that Element stores conversations on server is not a problem for me as long as the data is securely encrypted and the server operator cannot access it

<br/>

{{< cards >}}
  {{< card link="/en/data-abuse" title="Data abuse" icon="thumb-down" subtitle="by big tech companies. How does it work + examples" >}}
{{< /cards >}}
