---
linkTitle: Known Issues
title: Known Issues
weight: 7
toc: false
---

{{% details title="\"You have unverified sessions\" warning keeps popping up" closed="true" %}}

Mostly this is a relic of previous login attempts where you didn't complete the device verification, thus a false warning.

This occurs when you log in with a username and password but do not complete the session verification.

If you didn't log in recently, you can review the list of unverified sessions to check if there isn't something fishy (**`Review`** -> **`View All`**).

In the sessions list you can see which of them are unverified, what device, date and IP addresses they are from.

In general it would be wrong if someone logged into your account from a device type you don't recognise, an unknown IP address* and/or at a time when you definitely weren't trying to log in...

You can't go wrong by clicking on an unverified session and choosing **`Sign out of this session`**.

If this warning kept popping up repeatedly for no apparent reason, it would mean that someone has your username and password, is logging into your account but doesn't have the Security Key and is unable to decrypt the content of your account.

_*) IP addresses are changing depending on where you log in from, whether you use fixed or mobile internet, Wi-Fi outside your home, VPN, etc._

{{% /details %}}

{{% details title="Notifications don't work" closed="true" %}}

On some phones notifications may not work or work unreliably. So far I've met this with some Samsungs.

In general, Android's "battery optimization" should be turned off for Element.

This can be set (depending on the device and Android version) by pressing the Element icon, tapping on the "{{< icon "information-circle" >}}" (info) icon which takes you to the app settings (at the Android level). There you need to select **`Advanced`** -> **`Battery`** -> **`Battery Optimization (usage)`** and turn it off for Element.

Element also has an inbuilt notification troubleshooting feature - **`Settings`** -> **`Notifications`** -> at the very bottom **`Troubleshoot Notifications`**

{{% /details %}}

{{% details title="I don't see the \"Secure Backup\" option" closed="true" %}}

I encountered a situation where after installing the app and creating the account the "Secure Backup" option wasn't available.

This happened with the app installed from FDroid.

I think that the option appeared later but I don't have the exact procedure to resolve this at the moment.

If you have any insights on this let me know at element.entree929@aleeas.com.

{{% /details %}}
