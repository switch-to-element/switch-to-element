---
linkTitle: Link Another Device
title: Link Another Device
weight: 3
toc: false
---

Each new login still needs to be verified.

The easiest way is to do so from another device you're already logged into.





{{% details title="Mobile -> Desktop" closed="true" %}}

_I'm logged in on my mobile and want to log into my desktop_

||||
|-|-|-|
| 1 | Desktop | Log in with your username and password, you will be prompted to **Verify this device** |
| 2 | Desktop | If you have the Security Key stored in your password manager and can easily copy it over, select **`Verify with Security Key`**, otherwise choose **`Verify with another device`** |
| 3 | Mobile  | The **Verification Request** bar will pop up, tap it and choose **`Accept`** |
| 4 | Desktop | A QR code will appear |
| 5 | Mobile  | An option to **`Scan with this device`** will appear, scan the QR code |
| 6 | Desktop | Confirm whether you see the same image on your mobile |

{{% /details %}}





{{% details title="Desktop -> Mobile" closed="true" %}}

_I'm logged in on my desktop and want to log into my mobile_

||||
|-|-|-|
| 1 | Mobile   | Log in with your username and password |
| 2 | Mobile   | The **Verify this device** bar will pop up, tap it |
| 3 | Mobile   | If you have the Security Key stored in your password manager and can easily copy it over, select **`Use a Recovery Passphrase or Key`**, otherwise choose **`Verify with another device`** |
| 4 | Desktop  | A **Verification requested** notifitaion will pop up; click on **`Verify Session`**, a QR code will appear |
| 5 | Mobile   | An option to **`Scan with this device`** will appear, scan the QR code |
| 6 | Desktop  | Confirm whether you see the same image on your mobile |
| 7 | Mobile   | Conversations in your account may not load immediately. If you don't see them, close (kill) Element and reopen it. |

{{% /details %}}





{{% details title="Desktop -> Desktop" closed="true" %}}

_I'm logged in on one desktop and want to log into another desktop_

||||
|-|-|-|
| 1 | New desktop      | Log in with your username and password, you will be prompted to **Verify this device** |
| 2 | New desktop      | If you have the Security Key stored in your password manager and can easily copy it over, select **`Verify with Security Key`**, otherwise choose **`Verify with another device`** |
| 3 | Original desktop | A **Verification requested** notifitaion will pop up; click on **`Verify Session`** |
| 4 | Both desktops    | A window will pop up asking you to **Compare unique emoji set** |
| 5 | Both desktops    | Check and confirm whether the emoji on both desktops match and are displayed in the same order |

_(I don't get how this method of authentication/Security Key transfer works. If anyone can explain it to me, please email element.entree929@aleeas.com)_

{{% /details %}}





{{% details title="Mobile -> Mobile" closed="true" %}}

_I'm logged in on one mobile and want to log into another mobile_

||||
|-|-|-|
| 1 | New mobile       | Log in with your username and password |
| 2 | New mobile       | The **Verify this device** bar will pop up, tap it |
| 3 | New mobile       | If you have the Security Key stored in your password manager and can easily copy it over, select **`Use a Recovery Passphrase or Key`**, otherwise choose **`Verify with another device`** |
| 4 | Original mobile  | The **Verification Request** bar will pop up, tap it and choose **`Accept`**, a QR code will appear on both mobiles |
| 5 | Any mobile       | An option to **`Scan with this device`** will appear on both mobiles, scan the QR code from the other one |
| 6 | The other mobile | Confirm whether you see the same image on both mobiles |
| 7 | New mobile       | Conversations in your account may not load immediately. If you don't see them, close (kill) Element and reopen it. |

{{% /details %}}





{{% details title="I'm not logged in anywhere at the moment" closed="true" %}}

In such case the only way to verify your device is by inserting the Security Key.

{{% /details %}}





## What's the purpose of device verification

Each time you log into your account, the Security Key must be transferred/inserted to decrypt its contents.

"Verification" is nothing more than transferring this key, just in a more user-friendly way.

If you're not logged in anywhere, you have no choice but to insert the entire security key.
