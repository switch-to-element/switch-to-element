---
linkTitle: Element je lepší
weight: 6
---

{{< hextra/feature-grid >}}
  {{< hextra/feature-card
    title="Decentralizovaný"
    subtitle="Neregistruješ se u jediné firmy, ale můžeš si vybrat, u koho se zaregistruješ. Ke svému účtu můžeš přistupovat i pomocí jiných aplikací, než jen Elementu."
  >}}
  {{< hextra/feature-card
    title="Chrání soukromí"
    subtitle="Obsah účtu je šifrovaný, při registraci zadáváš jen e-mail, pro fungování nepotřebuje přístup ke kontaktům. Nesbírá informace, které nemusí."
  >}}
  {{< hextra/feature-card
    title="Otevřený"
    subtitle="Lze ověřit, co aplikace skutečně dělá, a v případě, že by se současný vývojář začal chovat neeticky, je možné a legální aplikaci zduplikovat a špatné změny zahodit."
  >}}
{{< /hextra/feature-grid >}}

<br/>

# Element je lepší než -

## WhatsApp (a Facebook Messenger)

- Patří Metě (Facebooku) - o tom, co je Meta zač, si můžeš přečíst víc [zde](/data-abuse)
- Obsah konverzací by sice *měl* být šifrovaný, ale k metadatům a kontaktům má Meta přístup každopádně
  - **Metadata** jako *s kým* komunikuješ, *jak často*, *z jakých zařízení, IP adres*... jsou také vysoce vypovídající a citlivé osobní údaje
  - Pokud aplikace nemá otevřený zdrojový kód, není možné ověřit, že provozovatel nemá přístup k obsahu konverzací navzdory koncovému šifrování. Existují informace, že konverzace jsou monitorovány (viz níže).
- **Aplikaci nelze používat, aniž by měla přístup ke kontaktům v telefonu** - Meta si vynucuje přístup k aktuálnímu seznamu tvých kontaktů
- Převzetí WhatsAppu Facebookem je smutný příběh, jenž vrcholí doporučením zakladatele WA "Smažte si Facebook" a jeho podporou Signalu coby konkurenční platformě

{{% details title="Další argumenty proti WhatsAppu" closed="true" %}}

- DON'T Use WhatsApp! - [peertube.tv](https://peertube.tv/w/8suSZ4ojg2Q2ok8HMmn6T5), [youtube.com](https://www.youtube.com/watch?v=shpiVm1qpnw)
- [Proof that WhatsApp isn't private!](https://www.youtube.com/watch?v=sqb37P4vbrc&t=130s)
- How Whatsapp (and Facebook) will Zuck You - [odysee.com](https://odysee.com/@RobBraxmanTech:6/how-whatsapp-and-facebook-will-zuck-you:e), [youtube.com](https://www.youtube.com/watch?v=I6Fbjr2WfFw)
- [Why You Should Delete Whatsapp](https://www.youtube.com/watch?v=bP5nXvzZMcE)
- [Delete WhatsApp Now](https://www.youtube.com/watch?v=KAsoDwpO2OU)
- Pozadí převzetí WhatsAppu Facebookem - [buzzfeednews.com](https://www.buzzfeednews.com/article/ryanmac/whatsapp-brian-acton-delete-facebook-stanford-lecture), [forbes.com](https://www.forbes.com/sites/parmyolson/2018/09/26/exclusive-whatsapp-cofounder-brian-acton-gives-the-inside-story-on-deletefacebook-and-why-he-left-850-million-behind/)

{{% /details %}}

## Telegram

- Principiálně není lepší než WhatsApp nebo Facebook Messenger - je to uzavřená centralizovaná platforma
- Konverzace nejsou ve výchozím stavu šifrovány a Telegram k nim tedy má přístup
- Jeho záběr je příliš široký, zahlcuje zbytečnými funkcemi, v nastaveních je těžké se vyznat a ohlídat si to důležité
- Vyžaduje telefonní číslo

## Signal

- Soukromí/bezpečnost zde už je (pro naše účely) příliš na úkor použitelnosti
  - Pro mě konkrétně je zásadním problémem nemožnost synchronizace více jak 2 zařízení (a ani na těch mi synchronizace nefungovala uspokojivě)
    - To, že Element uchovává konverzace na serveru, pro mě není problém, pokud jsou data bezpečně zašifrována a provozovatel serveru k nim nemá přístup

<br/>

{{< cards >}}
  {{< card link="/data-abuse" title="Zneužívání dat" icon="thumb-down" subtitle="big tech firmami. Jak funguje + příklady" >}}
{{< /cards >}}
