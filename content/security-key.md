---
linkTitle: Bezpečnostní klíč
title: Bezpečnostní/šifrovací klíč
weight: 2
---

K plnému přihlášení do Elementu nestačí jen jméno a heslo, ale je třeba ještě bezpečnostní klíč.

Je to proto, že zprávy jsou šifrované a návod, jak je dešifrovat, nemá nikdo jiný. Bezpečnostní/šifrovací klíč je právě tímto návodem.

Proto je třeba, na rozdíl od mainstreamových messengerů, aby sis jej uložil/a a uchovával/a sám/a. Pokud jej uchováš v tajnosti, máš záruku, že k obsahu tvých zpráv se nikdo nedostane.

## Jak klíč správně uchovat

Nejpřímočařejší cesta je klíč opsat a uložit mezi ostatní důležité dokumenty jako rodný list nebo maturitní vysvědčení.

V pohodě je také uložit ho do správce hesel, i když tady už pozor na zabezpečení, protože pokud by se ti někdo do správce dostal, tak bude mít k dispozici všechny 3 údaje potřebné k převzetí účtu (jméno, heslo a klíč).

## Jak klíč určitě neuchovávat

Určitě nedávat na žádný cloud (Google Drive, Google Photos, OneDrive, DropBox, iCloud...).

Jakmile se něco jednou dostane do cloudu, tak už nad tím nikdy nebudeš mít kontrolu. Že něco smažeš z cloudu neznamená, že je to nenávratně smazáno ze serveru. Jen to nevidíš.

Nedělej screenshot, klíč nefoť. Odtud je jenom krůček k (automatickému) nahrání do cloudu.
